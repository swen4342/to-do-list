
var counter = 0;
var complete = 0;
var remain = 0;
/*
var config = {
    apiKey: "AIzaSyCpX-69BW2GUF7eTqwluMb1c4ldGh5hCHk",
    authDomain: "to-do-list-application-b30a7.firebaseapp.com",
    databaseURL: "https://to-do-list-application-b30a7.firebaseio.com",
    projectId: "to-do-list-application-b30a7",
    storageBucket: "to-do-list-application-b30a7.appspot.com",
    messagingSenderId: "167120485121"
};
firebase.initializeApp(config);

var database = firebase.database();
var ref = database.ref('user');
*/
function setPage()
{
    if(localStorage.getItem('page') === null)
    {
        savePage();
    }
    else{
        loadPrevious();
    }
}    

function logIn() 
{
    var username = document.getElementById('user-name').value;
    var password = document.getElementById('password').value;
    
    if(username == "group13" || password == "swen4342")
    document.location.href = "list.html";
}
/*
function checkUser(user, password) 
{
    alert(ref.child('user').val());
}
*/
/*
function newUser()
{
    document.location.href = "newuser.html";
}
*/
/*
function submitUser()
{
    var username = document.getElementById('new-user-name').value;
    var password = document.getElementById('new-password').value;
    var verifyPassword = document.getElementById('verify-password').value;

    if(username == '' && password == '' )
    {
        alert('please make valid entry');
    }
    else if(password == verifyPassword)
    {
        var data = {
            name: username,
            password: password
        }
    
        ref.push(data);
        document.location.href = "index.html";
    }
    else
    {
        alert('please enter valid password');
    }
}
*/

function setCounters()
{
    counter = localStorage.getItem('counter');
    remain = localStorage.getItem('remain');
    complete = localStorage.getItem('complete');

    if(localStorage.getItem('remain') === null)
    {
        document.getElementById('remain').innerHTML = "To Do's: 0";
    }
    else
    {
        document.getElementById('remain').innerHTML = "To Do's: " + remain;
    }    

    if(localStorage.getItem('complete') === null)
    {
        document.getElementById('complete').innerHTML = "Completed To Do's: 0";
    }
    else
    {
        document.getElementById('complete').innerHTML = "Completed To Do's: " + complete;
    } 
}

function addToDo(event, listID, elementID)
{
    var key = window.event;
    var list = listID;
    var input = document.getElementById(elementID).value;
    var itemString;

    if(key.keyCode == 13)
    {
        document.getElementById(list).innerHTML += "<div id='todo' class='todo-container'><button type='button' id='remove' class='check' onclick='removeToDo(this); /*addComplete();*/'> &#10004 </button><h3 class='todo'>" + input + "</h3></div>";

        updatePage();

        counter ++;
        localStorage.setItem('counter', counter);

        remain ++;
        localStorage.setItem('remain', remain);
        document.getElementById('remain').innerHTML = "To Do's: " + remain;
    }
}

function removeToDo(element)
{
    key = element.parentNode.children[1].textContent;

    element.parentNode.remove();

    updatePage();

    counter --;
    localStorage.setItem('counter', counter);

    remain--;
    localStorage.setItem('remain', remain);
    document.getElementById('remain').innerHTML = "To Do's: " + remain;

    complete++;
    localStorage.setItem('complete', complete);
    document.getElementById('complete').innerHTML = "To Do's: " + complete;
}

function clearList()
{
    for(var i = 0; i < counter; i++)
    {
        if(document.getElementById('todo') != null)
        {
            document.getElementById('todo').remove();
        }
    }

    counter = 0;
    remain = 0;
    complete = 0;

    updatePage();

    localStorage.setItem('counter', 0);
    localStorage.setItem('remain', 0);
    document.getElementById('remain').innerHTML = "To Do's: " + remain;
    localStorage.setItem('complete', 0);
    document.getElementById('complete').innerHTML = "To Do's: " + complete;

}

function updatePage()
{
    var doc = document.documentElement.innerHTML;

    localStorage['page'] = doc;
}

function savePage()
{
    localStorage.setItem('page', document.documentElement.innerHTML);
}


function loadPrevious()
{
    document.querySelector('html').innerHTML = localStorage['page'];
}

